#!/bin/bash

# Extract macOS version from the image name
macos_version_from_image=$(echo "$CI_JOB_IMAGE" | sed -n 's/^macos-\([0-9]*\).*/\1/p')

# Get macOS version using sw_vers
installed_macos_version_long=$(sw_vers -productVersion)
installed_macos_version=$(echo "$installed_macos_version_long" | cut -d. -f1)

echo "macOS version from image: $macos_version_from_image"
echo "Installed macOS version: $installed_macos_version_long"

# Compare the extracted macOS versions
if [ "$macos_version_from_image" = "$installed_macos_version" ]; then
    echo "macOS Version matches!"
else
    echo "Version does not match."
    exit 1
fi
